// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package upload2gcs

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// EntryPoint function entry point.
func EntryPoint(ctxEvent context.Context, pubsubMsg pubsub.Message) error {
	b, _ := json.MarshalIndent(pubsubMsg, "", "  ")

	var ev glo.EntryValues
	ev.CommonEntryValues = global.CommonEv

	msgTimestamp, err := time.Parse(time.RFC3339, pubsubMsg.Attributes["timestamp"])
	if err != nil {
		glo.LogCriticalNoRetry(ev, "triggering PubSub message attribute 'timestamp' is missing or invalid and should not", "", "")
		return nil
	}

	now := time.Now()
	d := now.Sub(msgTimestamp)

	glo.LogStartCloudEvent(ev, b, d.Seconds(), &now)

	if pubsubMsg.Attributes["messageType"] != "asset_feed" {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("want triggering PubSub message attribute 'messageType' to be 'asset_feed' and got %s", pubsubMsg.Attributes["messageType"]), "", "")
		return nil
	}

	if pubsubMsg.Attributes["origin"] == "" {
		glo.LogCriticalNoRetry(ev, "triggering PubSub message attribute 'origin' is zero value and should not", "", "")
		return nil
	}

	if pubsubMsg.Attributes["assetType"] == "" {
		glo.LogCriticalNoRetry(ev, "triggering PubSub message attribute 'assetType' is zero value and should not", "", "")
		return nil
	}

	if pubsubMsg.Attributes["contentType"] == "" {
		glo.LogCriticalNoRetry(ev, "triggering PubSub message attribute 'contentType' is zero value and should not", "", "")
		return nil
	}

	ev.Step = glo.Step{
		StepID:        fmt.Sprintf("%s/%s/%s/%s/%s", pubsubMsg.Attributes["messageType"], pubsubMsg.Attributes["origin"], pubsubMsg.Attributes["assetType"], pubsubMsg.Attributes["contentType"], pubsubMsg.ID),
		StepTimestamp: msgTimestamp,
	}
	ev.StepStack = make(glo.Steps, 0)
	ev.StepStack = append(ev.StepStack, ev.Step)

	var feedMessage cai.FeedMessage
	err = json.Unmarshal(pubsubMsg.Data, &feedMessage)
	if err != nil {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("json.Unmarshal(pubsubMsg.Data, &feedMessage) %v", err), "", "")
		return nil
	}
	if feedMessage.StepStack != nil {
		ev.StepStack = append(feedMessage.StepStack, ev.Step)
	} else {
		var caiStep glo.Step
		caiStep.StepTimestamp = feedMessage.Window.StartTime
		caiStep.StepID = fmt.Sprintf("%s/%s", feedMessage.Asset.Name, caiStep.StepTimestamp.Format(time.RFC3339))
		ev.StepStack = append(ev.StepStack, caiStep)
		ev.StepStack = append(ev.StepStack, ev.Step)
	}
	feedMessage.StepStack = ev.StepStack

	origin := feedMessage.Origin
	var finishMsg, finishMsgDescription, objectNameSuffix string

	// a := make([]byte, 1)
	a := feedMessage.Asset.IamPolicy
	if len(a) > 4 {
		objectNameSuffix = "_iam.json"
	} else {
		objectNameSuffix = ".json"
	}
	objectName := strings.Replace(feedMessage.Asset.Name, "/", "", 2) + objectNameSuffix
	storageObject := global.bucketHandle.Object(objectName)

	if feedMessage.Deleted {
		err = storageObject.Delete(ctxEvent)
		if err != nil {
			if strings.Contains(err.Error(), "object doesn't exist") {
				glo.LogWarning(ev, "already deleted", fmt.Sprintf("object doesn't exist, cannot delete %s", objectName))
				return nil
			}
			glo.LogWarning(ev, "storageObject.Delete", err.Error())
			return nil
		}
		finishMsg = "delete object"
		finishMsgDescription = objectName
	} else {
		content, err := json.MarshalIndent(feedMessage.Asset, "", "    ")
		if err != nil {
			glo.LogCriticalNoRetry(ev, fmt.Sprintf("json.MarshalIndent(feedMessage.Asset %v", err), feedMessage.Asset.AssetType, "")
			return nil
		}
		storageObjectWriter := storageObject.NewWriter(ctxEvent)
		_, err = fmt.Fprint(storageObjectWriter, string(content))
		if err != nil {
			glo.LogCriticalRetry(ev, fmt.Sprintf("fmt.Fprint(storageObjectWriter, string(content)) %v", err), feedMessage.Asset.AssetType, "")
			return err
		}
		err = storageObjectWriter.Close()
		if err != nil {
			glo.LogCriticalRetry(ev, fmt.Sprintf("storageObjectWriter.Close() %v", err), feedMessage.Asset.AssetType, "")
			return err
		}
		finishMsg = "upload object"
		finishMsgDescription = objectName
	}
	glo.LogFinish(ev, finishMsg, finishMsgDescription, time.Now(), origin, feedMessage.Asset.AssetType, feedMessage.ContentType, "", 0)
	return nil
}
