// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package upload2gcs

import (
	"cloud.google.com/go/storage"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

const microserviceName = "upload2gcs"

// ServiceEnv list environment variables prefixed with the name of the microservice
type ServiceEnv struct {
	AssetJSONBucketName   string `envconfig:"asset_json_bucket_name"`
	Environment           string `envconfig:"environment" default:"dev"`
	LogOnlySeveritylevels string `envconfig:"log_only_severity_levels" default:"WARNING NOTICE CRITICAL"`
	ProjectID             string `envconfig:"project_id" required:"true"`
	StartProfiler         bool   `envconfig:"start_profiler" default:"false"`
}

// Env list environment variables
type Env struct {
	KConfiguration string `envconfig:"k_configuration"`
	KRevision      string `envconfig:"k_revision"`
	KService       string `envconfig:"k_service"`
}

// Global structure for global variables to optimize the performances in serverless mode
type Global struct {
	bucketHandle *storage.BucketHandle
	env          *Env
	serviceEnv   *ServiceEnv
	CommonEv     glo.CommonEntryValues
}
